import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { WeatherForcast } from "../models/weatherforcast";

interface Props {
    buttonClicked: boolean;
}

const WeatherForcastTable = (props: Props) => {
    const [data, setData] = useState<WeatherForcast[] | undefined>();
    const [errorMessage, setErrorMessage] = useState<string>();

    useEffect(() => {
        setData(undefined);
        fetch(`${process.env.REACT_APP_BASE_URL}/WeatherForecast`)
            .then(response => response.json())
            .then(json => {
                setData(json);
                setErrorMessage(undefined);
            })
            .catch(error => {
                setErrorMessage(error.message)
            });
    }, [props.buttonClicked]);

    return <div>
        {data === undefined
            ? "Loading..."
            : errorMessage ?? <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>TempC</th>
                        <th>TempF</th>
                        <th>Summary</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(wf => {
                        return <tr>
                            <td>{data.indexOf(wf) + 1}</td>
                            <td>{wf.date + ""}</td>
                            <td>{wf.temperatureC}</td>
                            <td>{wf.temperatureF}</td>
                            <td>{wf?.summary}</td>
                        </tr>
                    })}
                </tbody>
            </Table>}
    </div>;
};

export default WeatherForcastTable;