import { Button } from "react-bootstrap";
import WeatherForcastTable from "../components/weatherforcast-table";
import { useState } from "react";



const Home= () => {
    const [buttonClicked, setButtonClicked] = useState<boolean>(false);

    const onClick = () => {
        setButtonClicked(!buttonClicked)
    };

    return <div>
        <Button variant="outline-primary" onClick={onClick}>Получить прогноз погоды</Button>
        <hr></hr>
        <WeatherForcastTable buttonClicked={buttonClicked}></WeatherForcastTable>
    </div>;
};

export default Home;