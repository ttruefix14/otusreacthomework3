export interface WeatherForcast {
    date: string;
    temperatureC: number;
    temperatureF: number;
    summary?: string;
}